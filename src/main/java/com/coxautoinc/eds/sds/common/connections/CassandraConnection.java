package com.coxautoinc.eds.sds.common.connections;

import com.coxautoinc.eds.common.DefaultValue;
import com.datastax.driver.core.CloseFuture;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.ConstantReconnectionPolicy;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by akgillella on 7/25/16.
 */
@Singleton
public class CassandraConnection {

    private final static Logger LOGGER = LoggerFactory.getLogger(CassandraConnection.class);

    @Inject
    @DefaultValue(key = "cassandra.contactPoints", value = "localhost")
    private String[] hosts;

    @Inject
    @DefaultValue(key = "cassandra.port", value = "9042")
    private Integer port;

    @Inject
    @DefaultValue(key = "cassandra.user", value = " ")
    private String user;

    @Inject
    @DefaultValue(key = "cassandra.password", value = " ")
    private String password;

    @Inject
    @DefaultValue(key = "cassandra.connection.timeout", value="3000")
    private Integer timeout;

    @Inject
    @DefaultValue(key = "cassandra.connection.readTimeout", value="15000")
    private Integer readTimeout;

    @Inject
    @DefaultValue(key = "cassandra.pool.local.coreConnections", value="1")
    private Integer localConnections;

    @Inject
    @DefaultValue(key = "cassandra.pool.local.maxConnections", value="2")
    private int localMaxConnections;

    @Inject
    @DefaultValue(key = "cassandra.pool.remote.coreConnections", value="0")
    private Integer remoteConnections;

    @Inject
    @DefaultValue(key = "cassandra.pool.remote.maxConnections", value="0")
    private Integer remoteMaxConnections;

    @Inject
    @DefaultValue(key = "cassandra.pool.size", value = "5")
    private Integer poolSize;

    @Inject
    @DefaultValue(key = "cassandra.shutdown.time", value = "10000")
    private long shutdownTimeout;

    private TimeUnit shutdownTimeoutUnit = TimeUnit.MILLISECONDS;

    private Session session = null;

    private Cluster cluster = null;

    @PostConstruct
    public void init() {

        LOGGER.info("Initializing configurable Cassandra connection pool  : {}:{}:{}:{}:{}",
                hosts, port, timeout, readTimeout, poolSize);

        Cluster.Builder builder = null;

        if (StringUtils.isEmpty(user) && StringUtils.isEmpty(password)) {
            builder = Cluster.builder()
                    .withPort(port);
        } else {
            builder = Cluster.builder()
                    .withCredentials(user, password)
                    .withPort(port);
        }

        for (String host : hosts) {
            builder.addContactPoint(host);
        }

        cluster = builder.withReconnectionPolicy(new ConstantReconnectionPolicy(3000L)).build();
        cluster.getConfiguration().getPoolingOptions()
                .setCoreConnectionsPerHost(HostDistance.LOCAL, localConnections)
                //.setMaxConnectionsPerHost(HostDistance.LOCAL, localMaxConnections)
                .setCoreConnectionsPerHost(HostDistance.REMOTE, remoteConnections)
                .setMaxConnectionsPerHost(HostDistance.REMOTE, remoteMaxConnections);
        cluster.getConfiguration().getSocketOptions()
                .setConnectTimeoutMillis(timeout)
                .setReadTimeoutMillis(readTimeout);
        cluster.init();

        LOGGER.info("Completed Initializing configurable Cassandra connection pool for Ingestion Store : {}:{}:{}:{}", hosts, port, timeout, poolSize);

    }

    /**
     * Get a Cassandra session from the resource pool
     *
     * @return {@link com.datastax.driver.core.Session}
     */
    public Session getSession() {

        if (session == null) {
            session = cluster.connect();
        }
        return session;
    }

    @PreDestroy
    private void close() {
        try{

            if (session != null && !session.isClosed()) {
                session.close();
                CloseFuture shutdown = cluster.closeAsync();
                try {
                    LOGGER.debug("waiting for Cassandra Cluster to shutdown.");
                    shutdown.get(shutdownTimeout, shutdownTimeoutUnit);
                } catch (TimeoutException ex) {
                    LOGGER.error("Cassandra Cluster shutdown has timed out, attempting to force the shutdown and waiting for 10% of the original timeout");
                    shutdown.force();
                    shutdown.get(shutdownTimeout / 10, shutdownTimeoutUnit);
                } finally {
                    if (!shutdown.isDone()) {
                        LOGGER.error("Cassandra Cluster shutdown has timed out and not finished. Giving Up!");
                    } else {
                        LOGGER.info("Cassandra Cluster has shutdown");
                    }
                }
            }

        }catch(Throwable t){
            t.printStackTrace();
        }

    }
}
