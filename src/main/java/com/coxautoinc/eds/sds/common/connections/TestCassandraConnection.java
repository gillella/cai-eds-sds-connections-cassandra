package com.coxautoinc.eds.sds.common.connections;

import com.datastax.driver.core.Session;
import org.apache.deltaspike.cdise.api.CdiContainer;
import org.apache.deltaspike.cdise.api.CdiContainerLoader;
import org.apache.deltaspike.cdise.api.ContextControl;
import org.apache.deltaspike.core.api.provider.BeanProvider;

import javax.inject.Inject;

/**
 * Created by akgillella on 7/26/16.
 */
public class TestCassandraConnection {

    @Inject
    CassandraConnection connection;


    public Session getCassandraSession(){

        return connection.getSession();
    }


    public static void main(String[] args){


        // CDI container initialization
        CdiContainer cdiContainer = CdiContainerLoader.getCdiContainer();
        cdiContainer.boot();
        ContextControl contextControl = cdiContainer.getContextControl();
        contextControl.startContexts();

        final TestCassandraConnection con = BeanProvider.getContextualReference(TestCassandraConnection.class);

        Session session = con.getCassandraSession();

        System.out.println("Session state :"+session.getState().getConnectedHosts().toString());

        System.out.println("Session keyspace :"+session.getLoggedKeyspace());


    }
}
